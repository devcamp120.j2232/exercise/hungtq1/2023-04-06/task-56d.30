package com.devcamp.restapiartistalbum.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.restapiartistalbum.models.Artist;

@Service
public class ArtistService {
  @Autowired
  private AlbumService albumService;

  Artist artist1 = new Artist(1, "Huynh Ngoc Hue");
  Artist artist2 = new Artist(2, "Quach Dinh Phu");
  Artist artist3 = new Artist(3, "Thai Thien Thanh");

  ArrayList<Artist> artists = new ArrayList<>();

  public ArrayList<Artist> getArtists() {
    return artists;
  }

  public ArrayList<Artist> getAllArtistService() {
    artist1.setAlbums(albumService.getAlbums1());
    artist2.setAlbums(albumService.getAlbums2());
    artist3.setAlbums(albumService.getAlbums3());

    ArrayList<Artist> allArtists = new ArrayList<>();

    allArtists.add(artist1);
    allArtists.add(artist2);
    allArtists.add(artist3);

    artists.addAll(allArtists);

    return allArtists;
  }

  public Artist getArtistId(int ArtistCode) {
    Artist artistResult = new Artist();
    for (int counter = 0; counter < artists.size(); counter++) {
      Artist ArtistCheck = artists.get(counter);
      if (ArtistCode == ArtistCheck.getId()) {
        artistResult = ArtistCheck;
      }
    }
    return artistResult;
  }

  public Artist getIndexArtist(int index) {
    Artist artist = null;
    if (index >= 0 && index <= artists.size())
      artist = artists.get(index);
    return artist;
  }

}
