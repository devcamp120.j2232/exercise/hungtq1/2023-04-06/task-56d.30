package com.devcamp.restapiartistalbum.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.restapiartistalbum.models.Album;

@Service
public class AlbumService {
  String song1 = "cuoi con duong";
  String song2 = "tinh mot dem";
  String song3 = "hen mot mai";
  String song4 = "cuoc doi";
  String song5 = "ly cay thong";
  String song6 = "em la cua anh";
  String song7 = "be len ba";
  String song8 = "tinh cha";
  String song9 = "tinh me";

  ArrayList<Album> albums = new ArrayList<>();

  public ArrayList<Album> getAlbums() {
    return albums;
  }

  public ArrayList<Album> getAlbums1() {
    ArrayList<String> songAll1 = new ArrayList<>();
    songAll1.add(song1);
    songAll1.add(song2);

    ArrayList<String> songAll2 = new ArrayList<>();
    songAll2.add(song2);
    songAll2.add(song3);

    ArrayList<String> songAll3 = new ArrayList<>();
    songAll3.add(song3);
    songAll3.add(song4);

    Album album1 = new Album(1, "Dam Vinh Hung", songAll1);
    Album album2 = new Album(2, "Toc Tien", songAll2);
    Album album3 = new Album(3, "My Tam", songAll3);
    ArrayList<Album> albums1 = new ArrayList<>();
    albums1.add(album1);
    albums1.add(album2);
    albums1.add(album3);

    albums.addAll(albums1);
    return albums1;
  }

  public ArrayList<Album> getAlbums2() {
    ArrayList<String> songAll4 = new ArrayList<>();
    songAll4.add(song4);
    songAll4.add(song5);

    ArrayList<String> songAll5 = new ArrayList<>();
    songAll5.add(song5);
    songAll5.add(song6);

    ArrayList<String> songAll6 = new ArrayList<>();
    songAll6.add(song6);
    songAll6.add(song7);

    Album album4 = new Album(4, "Ha Ho", songAll4);
    Album album5 = new Album(5, "Ha Anh Tuan", songAll5);
    Album album6 = new Album(6, "Kim Ly", songAll6);
    ArrayList<Album> albums2 = new ArrayList<>();
    albums2.add(album4);
    albums2.add(album5);
    albums2.add(album6);

    albums.addAll(albums2);
    return albums2;
  }

  public ArrayList<Album> getAlbums3() {
    ArrayList<String> songAll7 = new ArrayList<>();
    songAll7.add(song7);
    songAll7.add(song8);

    ArrayList<String> songAll8 = new ArrayList<>();
    songAll8.add(song8);
    songAll8.add(song9);

    ArrayList<String> songAll9 = new ArrayList<>();
    songAll9.add(song9);
    songAll9.add(song1);

    Album album7 = new Album(4, "Vuong Chien", songAll7);
    Album album8 = new Album(5, "Bach Son", songAll8);
    Album album9 = new Album(6, "Le Quyen", songAll9);
    ArrayList<Album> albums3 = new ArrayList<>();
    albums3.add(album7);
    albums3.add(album8);
    albums3.add(album9);

    albums.addAll(albums3);
    return albums3;
  }

  public Album getAlbumId(int AlbumCode) {
    Album albumResult = new Album();
    for (int counter = 0; counter < albums.size(); counter++) {
      Album albumCheck = albums.get(counter);
      if (AlbumCode == albumCheck.getId()) {
        albumResult = albumCheck;
      }
    }
    return albumResult;
  }
}
