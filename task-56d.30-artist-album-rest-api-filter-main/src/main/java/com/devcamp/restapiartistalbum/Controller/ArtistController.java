package com.devcamp.restapiartistalbum.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapiartistalbum.models.Artist;
import com.devcamp.restapiartistalbum.service.ArtistService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ArtistController {
  @Autowired
  private ArtistService artistService;

  @GetMapping("/artists")
  public ArrayList<Artist> getAllArtist() {
    ArrayList<Artist> artists = artistService.getAllArtistService();
    return artists;
  }

  @GetMapping("/artist-info")
  public Artist getArtistInfor(@RequestParam(name = "artistId") int artistId) {
    return artistService.getArtistId(artistId);
  }

  @GetMapping("/artists/{index}")
  public Artist getIndexArtist(@PathVariable int index) {
    return artistService.getIndexArtist(index);
  }

}
